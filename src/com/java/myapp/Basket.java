/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.java.myapp;

/**
 *
 * @author Supawee
 */
public class Basket {

    String ID;
    String name;
    int amount;
    int price;
    int cost ;//ทุน

    Basket(String ID, String name, int cost ,int price, int amount ) {
        this.ID = ID;
        this.name = name;
        this.cost = cost;
        this.price = price;
        this.amount = amount;
    }

    String getID() {
        return this.ID;
    }

    String getName() {
        return this.name;
    }

    int getCost() {
        return this.cost;
    }
    
    int getPrice() {
        return this.price;
    }

    int getAmount() {
        return this.amount;
    }

    void setAmount(int Amount) {
        this.amount = Amount;
    }
    
    void incAmount(int Amount) {
        this.amount += Amount;
    }
}
